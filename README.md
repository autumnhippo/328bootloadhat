# 328bootloadhat

Hat/shield to ease the process of burning an Arduino bootloader to an ATMega328 IC.

After assembly, simply plug the hat into an Arduino Uno or Nano, load the supplied sketch onto the Arduino, insert the ATMega328 chip to be bootloaded into the ZIF socket, and run the sketch.

Either have the PCB printed from the Gerber files, or:

[Buy it from the webshop](https://autumnhippo.com/products/328bootloadhat)

![Board](https://gitlab.com/autumnhippo/328bootloadhat/-/raw/main/media/board.jpg "Board")


## Parts List

* The PCB: Order it through your favourite PCB service by sending them the newest version of the gerber files.
* 1 x ZIF (zero-insertion-force) socket, 28 pins, narrow. Alternatively you can use a normal IC socket, but a ZIF socket is highly recommended.
* 2 x ceramic capacitor, 100 nF (marked "104"), part numbers C1, C2
* 2 x ceramic capacitor, 22 pF (marked "22"), part numbers C3, C4
* 1 x resistor, 10 KΩ, part number R1
* 1 x Crystal oscillator, 16 MHz, pin pitch 4.88 mm, part number Y1
* pin header connectors, male and female dependant on whether your Arduino has one or the other, obviously needs to be the opposite. If you have 10 male and 10 female header pins, you have plenty in either case.

## Assembly Instructions

1. Solder the resistor R1, all capacitors, C1-C4 and the crystal Y1 in place as per the silkscreen references and list above.
1. Solder the 6-pin header connectors to the ISCP pads on the backside of the board. Most Arduinos have male connectors for these -- if yours does as well, use female pin header connectors.
1. Solder a pin header connector to the "NANO_D10" and/or "UNO_D10", depending on whether you plan to use this hat with an Arduino Uno or Nano (or both). Obviously, use opposite gender of what your Arduino board has. It is recommended to solder the header connectors while there are in the Arduino, as it's otherwise very easy to get them slightly misaligned, thereby making it harder to insert the hat into the Arduino.
1. Turn the board back to the front side and solder the ZIF socket to the board. It's recommended to mark pin 1 on the ZIF socket with a permament marker so that it's very obvious which way ICs need to go.

![Mounted Board](https://gitlab.com/autumnhippo/328bootloadhat/-/raw/main/media/mounted.jpg "Mounted Board")

## Usage Instructions

1. Plug the 328bootloadhat into your Arduino Uno or Nano.
1. Get Nick Gammon's Arduino sketch from either [his Github page](https://github.com/nickgammon/arduino_sketches/tree/master/Atmega_Board_Programmer) or from the copy in this Gitlab page. Get the whole *Atmega_Board_Programmer* diretory and all files in it. Upload the *Atmega_Board_Programmer.ino* sketch to your Arduino.
1. Insert your ATMega328 chip into the ZIF socket. Be sure to align the notch/dot in the chip with the notch/dot on the silkscreen.
1. Run the sketch and open the Arduino IDE's Serial Monitor (be sure to select "Both NL & CR" and "115200 baud" in the bottom of the window).

[Gammon's webpage](http://www.gammon.com.au/bootloader) is an excellent resource if you experience problems with the sketch.

