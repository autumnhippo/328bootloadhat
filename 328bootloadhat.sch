<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Thomas">
<packages>
<package name="SINGLE_PIN_HEADERCONNECTOR">
<pad name="A" x="0" y="0" drill="0.9"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<text x="1.524" y="0" size="1.27" layer="25" align="center-left">&gt;NAME</text>
</package>
<package name="IC_ZIF_SOCKET_28P">
<pad name="1" x="-3.81" y="16.51" drill="1" shape="square"/>
<pad name="2" x="-3.81" y="13.97" drill="1"/>
<pad name="3" x="-3.81" y="11.43" drill="1"/>
<pad name="4" x="-3.81" y="8.89" drill="1"/>
<pad name="5" x="-3.81" y="6.35" drill="1"/>
<pad name="6" x="-3.81" y="3.81" drill="1"/>
<pad name="7" x="-3.81" y="1.27" drill="1"/>
<pad name="8" x="-3.81" y="-1.27" drill="1"/>
<pad name="9" x="-3.81" y="-3.81" drill="1"/>
<pad name="10" x="-3.81" y="-6.35" drill="1"/>
<pad name="11" x="-3.81" y="-8.89" drill="1"/>
<pad name="12" x="-3.81" y="-11.43" drill="1"/>
<pad name="13" x="-3.81" y="-13.97" drill="1"/>
<pad name="14" x="-3.81" y="-16.51" drill="1"/>
<pad name="15" x="3.81" y="-16.51" drill="1"/>
<pad name="16" x="3.81" y="-13.97" drill="1"/>
<pad name="17" x="3.81" y="-11.43" drill="1"/>
<pad name="18" x="3.81" y="-8.89" drill="1"/>
<pad name="19" x="3.81" y="-6.35" drill="1"/>
<pad name="20" x="3.81" y="-3.81" drill="1"/>
<pad name="21" x="3.81" y="-1.27" drill="1"/>
<pad name="22" x="3.81" y="1.27" drill="1"/>
<pad name="23" x="3.81" y="3.81" drill="1"/>
<pad name="24" x="3.81" y="6.35" drill="1"/>
<pad name="25" x="3.81" y="8.89" drill="1"/>
<pad name="26" x="3.81" y="11.43" drill="1"/>
<pad name="27" x="3.81" y="13.97" drill="1"/>
<pad name="28" x="3.81" y="16.51" drill="1"/>
<wire x1="-2.54" y1="17.78" x2="-2.54" y2="-17.78" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-17.78" x2="2.54" y2="-17.78" width="0.127" layer="21"/>
<wire x1="2.54" y1="-17.78" x2="2.54" y2="17.78" width="0.127" layer="21"/>
<wire x1="1.27" y1="17.78" x2="-1.27" y2="17.78" width="0.127" layer="21" curve="-180"/>
<text x="0" y="8.89" size="1.27" layer="25" rot="R90" align="center">&gt;NAME</text>
<wire x1="-1.27" y1="17.78" x2="-2.54" y2="17.78" width="0.127" layer="21"/>
<wire x1="1.27" y1="17.78" x2="2.54" y2="17.78" width="0.127" layer="21"/>
<circle x="-1.905" y="16.51" radius="0.381" width="0" layer="21"/>
<wire x1="7.62" y1="-26.67" x2="7.62" y2="22.86" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-26.67" x2="-7.62" y2="22.86" width="0.127" layer="21"/>
<wire x1="7.62" y1="-26.67" x2="-7.62" y2="-26.67" width="0.127" layer="21"/>
<wire x1="7.62" y1="22.86" x2="2.54" y2="22.86" width="0.127" layer="21"/>
<circle x="-8.89" y="21.59" radius="0.889" width="0" layer="21"/>
<wire x1="2.54" y1="22.86" x2="-2.54" y2="22.86" width="0.127" layer="21"/>
<wire x1="-2.54" y1="22.86" x2="-7.62" y2="22.86" width="0.127" layer="21"/>
<wire x1="1.27" y1="24.13" x2="-1.27" y2="24.13" width="0.127" layer="21" curve="-180"/>
<wire x1="1.27" y1="24.13" x2="2.54" y2="24.13" width="0.127" layer="21"/>
<wire x1="2.54" y1="24.13" x2="2.54" y2="22.86" width="0.127" layer="21"/>
<wire x1="-1.27" y1="24.13" x2="-2.54" y2="24.13" width="0.127" layer="21"/>
<wire x1="-2.54" y1="24.13" x2="-2.54" y2="22.86" width="0.127" layer="21"/>
<circle x="6.35" y="-20.32" radius="1.27" width="0.127" layer="21"/>
<wire x1="6.604" y1="-21.59" x2="6.604" y2="-22.86" width="0.127" layer="21"/>
<wire x1="6.604" y1="-22.86" x2="6.096" y2="-22.86" width="0.127" layer="21"/>
<wire x1="6.096" y1="-22.86" x2="6.096" y2="-21.59" width="0.127" layer="21"/>
<rectangle x1="-7.62" y1="-26.67" x2="7.62" y2="22.86" layer="39"/>
<text x="-7.874" y="16.51" size="1.27" layer="21" align="center-right">1</text>
<text x="-7.874" y="-16.51" size="1.27" layer="21" align="center-right">14</text>
<text x="7.874" y="16.51" size="1.27" layer="21" align="center-left">28</text>
<text x="7.874" y="-16.51" size="1.27" layer="21" align="center-left">15</text>
</package>
<package name="CAPACITOR_CERAMIC_THRUHOLE_P254">
<pad name="A" x="-1.27" y="0" drill="0.7"/>
<pad name="B" x="1.27" y="0" drill="0.7"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<text x="0" y="1.524" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<rectangle x1="-2.54" y1="-1.27" x2="2.54" y2="1.27" layer="39"/>
</package>
<package name="CAPACITOR_CERAMIC_THRUHOLE_P508">
<pad name="A" x="-2.54" y="0" drill="0.7"/>
<pad name="B" x="2.54" y="0" drill="0.7"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="-3.81" y2="1.27" width="0.127" layer="21"/>
<text x="0" y="1.524" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-1.27" x2="3.81" y2="1.27" layer="39"/>
</package>
<package name="RESISTOR_THRUHOLE">
<pad name="A" x="-3.81" y="0" drill="0.7"/>
<pad name="B" x="3.81" y="0" drill="0.7"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<text x="0" y="1.524" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<rectangle x1="-2.54" y1="-1.27" x2="2.54" y2="1.27" layer="39"/>
</package>
<package name="CRYSTAL_OSCILLATOR_TRUHOLE">
<pad name="A" x="-2.54" y="0" drill="0.9"/>
<pad name="B" x="2.54" y="0" drill="0.9"/>
<wire x1="3.175" y1="2.159" x2="3.175" y2="-2.159" width="0.127" layer="21" curve="-180"/>
<wire x1="-3.175" y1="-2.159" x2="-3.175" y2="2.159" width="0.127" layer="21" curve="-180"/>
<wire x1="-3.175" y1="2.159" x2="3.175" y2="2.159" width="0.127" layer="21"/>
<wire x1="3.175" y1="-2.159" x2="-3.175" y2="-2.159" width="0.127" layer="21"/>
<text x="0" y="1.905" size="1.27" layer="25" align="top-center">&gt;NAME</text>
<text x="0" y="-1.905" size="1.27" layer="27" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-3.556" y1="-2.159" x2="3.556" y2="2.159" layer="39"/>
<rectangle x1="-4.953" y1="-1.27" x2="-3.556" y2="1.27" layer="39"/>
<rectangle x1="3.556" y1="-1.27" x2="4.953" y2="1.27" layer="39"/>
</package>
<package name="ATMEGA328_DIP">
<pad name="1" x="-3.81" y="16.51" drill="1" shape="square"/>
<pad name="2" x="-3.81" y="13.97" drill="1"/>
<pad name="3" x="-3.81" y="11.43" drill="1"/>
<pad name="4" x="-3.81" y="8.89" drill="1"/>
<pad name="5" x="-3.81" y="6.35" drill="1"/>
<pad name="6" x="-3.81" y="3.81" drill="1"/>
<pad name="7" x="-3.81" y="1.27" drill="1"/>
<pad name="8" x="-3.81" y="-1.27" drill="1"/>
<pad name="9" x="-3.81" y="-3.81" drill="1"/>
<pad name="10" x="-3.81" y="-6.35" drill="1"/>
<pad name="11" x="-3.81" y="-8.89" drill="1"/>
<pad name="12" x="-3.81" y="-11.43" drill="1"/>
<pad name="13" x="-3.81" y="-13.97" drill="1"/>
<pad name="14" x="-3.81" y="-16.51" drill="1"/>
<pad name="15" x="3.81" y="-16.51" drill="1"/>
<pad name="16" x="3.81" y="-13.97" drill="1"/>
<pad name="17" x="3.81" y="-11.43" drill="1"/>
<pad name="18" x="3.81" y="-8.89" drill="1"/>
<pad name="19" x="3.81" y="-6.35" drill="1"/>
<pad name="20" x="3.81" y="-3.81" drill="1"/>
<pad name="21" x="3.81" y="-1.27" drill="1"/>
<pad name="22" x="3.81" y="1.27" drill="1"/>
<pad name="23" x="3.81" y="3.81" drill="1"/>
<pad name="24" x="3.81" y="6.35" drill="1"/>
<pad name="25" x="3.81" y="8.89" drill="1"/>
<pad name="26" x="3.81" y="11.43" drill="1"/>
<pad name="27" x="3.81" y="13.97" drill="1"/>
<pad name="28" x="3.81" y="16.51" drill="1"/>
<wire x1="-2.54" y1="17.78" x2="-2.54" y2="-17.78" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-17.78" x2="2.54" y2="-17.78" width="0.127" layer="21"/>
<wire x1="2.54" y1="-17.78" x2="2.54" y2="17.78" width="0.127" layer="21"/>
<wire x1="1.27" y1="17.78" x2="-1.27" y2="17.78" width="0.127" layer="21" curve="-180"/>
<text x="0" y="8.89" size="1.27" layer="25" rot="R90" align="center">&gt;NAME</text>
<wire x1="-1.27" y1="17.78" x2="-2.54" y2="17.78" width="0.127" layer="21"/>
<wire x1="1.27" y1="17.78" x2="2.54" y2="17.78" width="0.127" layer="21"/>
<rectangle x1="-2.54" y1="-17.78" x2="2.54" y2="17.78" layer="39"/>
<circle x="-1.651" y="16.51" radius="0.635" width="0" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="ATMEGA328">
<pin name="1" x="-7.62" y="15.24" visible="pad" length="short"/>
<pin name="2" x="-7.62" y="12.7" visible="pad" length="short"/>
<pin name="3" x="-7.62" y="10.16" visible="pad" length="short"/>
<pin name="4" x="-7.62" y="7.62" visible="pad" length="short"/>
<pin name="5" x="-7.62" y="5.08" visible="pad" length="short"/>
<pin name="6" x="-7.62" y="2.54" visible="pad" length="short"/>
<pin name="7" x="-7.62" y="0" visible="pad" length="short"/>
<pin name="8" x="-7.62" y="-2.54" visible="pad" length="short"/>
<pin name="9" x="-7.62" y="-5.08" visible="pad" length="short"/>
<pin name="10" x="-7.62" y="-7.62" visible="pad" length="short"/>
<pin name="11" x="-7.62" y="-10.16" visible="pad" length="short"/>
<pin name="12" x="-7.62" y="-12.7" visible="pad" length="short"/>
<pin name="13" x="-7.62" y="-15.24" visible="pad" length="short"/>
<pin name="14" x="-7.62" y="-17.78" visible="pad" length="short"/>
<pin name="15" x="7.62" y="-17.78" visible="pad" length="short" rot="R180"/>
<pin name="16" x="7.62" y="-15.24" visible="pad" length="short" rot="R180"/>
<pin name="17" x="7.62" y="-12.7" visible="pad" length="short" rot="R180"/>
<pin name="18" x="7.62" y="-10.16" visible="pad" length="short" rot="R180"/>
<pin name="19" x="7.62" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="20" x="7.62" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="21" x="7.62" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="22" x="7.62" y="0" visible="pad" length="short" rot="R180"/>
<pin name="23" x="7.62" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="24" x="7.62" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="25" x="7.62" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="26" x="7.62" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="27" x="7.62" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="28" x="7.62" y="15.24" visible="pad" length="short" rot="R180"/>
<wire x1="-5.08" y1="17.78" x2="-5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-20.32" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="5.08" y1="-20.32" x2="5.08" y2="17.78" width="0.254" layer="94"/>
<wire x1="5.08" y1="17.78" x2="-5.08" y2="17.78" width="0.254" layer="94"/>
<wire x1="0.762" y1="17.78" x2="-0.762" y2="17.78" width="0.254" layer="94" curve="-180"/>
<text x="0" y="18.034" size="1.27" layer="95" align="bottom-center">&gt;NAME</text>
<text x="4.826" y="15.494" size="0.6096" layer="94" align="bottom-right">Analog input 5</text>
<text x="4.826" y="12.954" size="0.6096" layer="94" align="bottom-right">Analog input 4</text>
<text x="4.826" y="10.414" size="0.6096" layer="94" align="bottom-right">Analog input 3</text>
<text x="4.826" y="7.874" size="0.6096" layer="94" align="bottom-right">Analog input 2</text>
<text x="4.826" y="5.334" size="0.6096" layer="94" align="bottom-right">Analog input 1</text>
<text x="4.826" y="2.794" size="0.6096" layer="94" align="bottom-right">Analog input 0</text>
<text x="4.826" y="0.254" size="0.6096" layer="94" align="bottom-right">GND</text>
<text x="4.826" y="-2.286" size="0.6096" layer="94" align="bottom-right">Analog reference</text>
<text x="4.826" y="-4.826" size="0.6096" layer="94" align="bottom-right">VCC</text>
<text x="4.826" y="-7.366" size="0.6096" layer="94" align="bottom-right">Digital pin 13</text>
<text x="4.826" y="-9.906" size="0.6096" layer="94" align="bottom-right">Digital pin 12</text>
<text x="4.826" y="-12.446" size="0.6096" layer="94" align="bottom-right">Digital pin 11 (PWM)</text>
<text x="4.826" y="-14.986" size="0.6096" layer="94" align="bottom-right">Digital pin 10 (PWM)</text>
<text x="4.826" y="-17.526" size="0.6096" layer="94" align="bottom-right">Digital pin 9 (PWM)</text>
<text x="-4.826" y="15.24" size="0.6096" layer="94" align="top-left">Reset</text>
<text x="-4.826" y="12.7" size="0.6096" layer="94" align="top-left">Digital pin 0 (RX)</text>
<text x="-4.826" y="10.16" size="0.6096" layer="94" align="top-left">Digital pin 1 (TX)</text>
<text x="-4.826" y="7.62" size="0.6096" layer="94" align="top-left">Digital pin 2</text>
<text x="-4.826" y="5.08" size="0.6096" layer="94" align="top-left">Digital pin 3 (PWM)</text>
<text x="-4.826" y="2.54" size="0.6096" layer="94" align="top-left">Digital pin 4</text>
<text x="-4.826" y="0" size="0.6096" layer="94" align="top-left">VCC</text>
<text x="-4.826" y="-2.54" size="0.6096" layer="94" align="top-left">GND</text>
<text x="-4.826" y="-5.08" size="0.6096" layer="94" align="top-left">Crystal 1</text>
<text x="-4.826" y="-7.62" size="0.6096" layer="94" align="top-left">Crystal 2</text>
<text x="-4.826" y="-10.16" size="0.6096" layer="94" align="top-left">Digital pin 5 (PWM)</text>
<text x="-4.826" y="-12.7" size="0.6096" layer="94" align="top-left">Digital pin 6 (PWM)</text>
<text x="-4.826" y="-15.24" size="0.6096" layer="94" align="top-left">Digital pin 7</text>
<text x="-4.826" y="-17.78" size="0.6096" layer="94" align="top-left">Digital pin 8</text>
</symbol>
<symbol name="SINGLE_PIN_CONNECTOR">
<pin name="A" x="-2.54" y="0" visible="off" length="short"/>
<wire x1="0" y1="-2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<text x="3.048" y="0" size="1.778" layer="95" align="center-left">&gt;NAME</text>
</symbol>
<symbol name="CAPACITOR_CERAMIC">
<pin name="A" x="-5.08" y="0" visible="off" length="middle"/>
<pin name="B" x="7.62" y="0" visible="off" length="middle" rot="R180"/>
<wire x1="0" y1="-2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<text x="1.27" y="-4.064" size="1.27" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="-0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<text x="0" y="1.27" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="CRYSTAL_OSCILLATOR">
<pin name="A" x="-7.62" y="0" visible="off" length="middle"/>
<pin name="B" x="7.62" y="0" visible="off" length="middle" rot="R180"/>
<wire x1="-1.778" y1="5.08" x2="-1.778" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-5.08" x2="1.778" y2="-5.08" width="0.254" layer="94"/>
<wire x1="1.778" y1="-5.08" x2="1.778" y2="5.08" width="0.254" layer="94"/>
<wire x1="1.778" y1="5.08" x2="-1.778" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<text x="0" y="0" size="1.27" layer="96" rot="R90" align="center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATMEGA328" prefix="IC">
<gates>
<gate name="G$1" symbol="ATMEGA328" x="0" y="2.54"/>
</gates>
<devices>
<device name="DIP" package="ATMEGA328_DIP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ZIFSOCKET" package="IC_ZIF_SOCKET_28P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SINGLE_PIN_CONNECTOR" prefix="CN">
<gates>
<gate name="G$1" symbol="SINGLE_PIN_CONNECTOR" x="0" y="0"/>
</gates>
<devices>
<device name="HEADER" package="SINGLE_PIN_HEADERCONNECTOR">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR_CERAMIC" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR_CERAMIC" x="0" y="0"/>
</gates>
<devices>
<device name="THRUHOLE_P254" package="CAPACITOR_CERAMIC_THRUHOLE_P254">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="THRUHOLE_P508" package="CAPACITOR_CERAMIC_THRUHOLE_P508">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="THRUHOLE" package="RESISTOR_THRUHOLE">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL_OSCILLATOR" prefix="Y" uservalue="yes">
<gates>
<gate name="G$1" symbol="CRYSTAL_OSCILLATOR" x="0" y="0"/>
</gates>
<devices>
<device name="TRUHOLE" package="CRYSTAL_OSCILLATOR_TRUHOLE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="ATMEGA328" library="Thomas" deviceset="ATMEGA328" device="ZIFSOCKET" value="ATMEGA328ZIFSOCKET"/>
<part name="MISO" library="Thomas" deviceset="SINGLE_PIN_CONNECTOR" device="HEADER"/>
<part name="SCK" library="Thomas" deviceset="SINGLE_PIN_CONNECTOR" device="HEADER"/>
<part name="VCC" library="Thomas" deviceset="SINGLE_PIN_CONNECTOR" device="HEADER"/>
<part name="MOSI" library="Thomas" deviceset="SINGLE_PIN_CONNECTOR" device="HEADER"/>
<part name="GND" library="Thomas" deviceset="SINGLE_PIN_CONNECTOR" device="HEADER"/>
<part name="UNO_D10" library="Thomas" deviceset="SINGLE_PIN_CONNECTOR" device="HEADER"/>
<part name="UNO_SUPPORT" library="Thomas" deviceset="SINGLE_PIN_CONNECTOR" device="HEADER"/>
<part name="RESET" library="Thomas" deviceset="SINGLE_PIN_CONNECTOR" device="HEADER"/>
<part name="NANO_SUPPORT" library="Thomas" deviceset="SINGLE_PIN_CONNECTOR" device="HEADER"/>
<part name="NANO_D10" library="Thomas" deviceset="SINGLE_PIN_CONNECTOR" device="HEADER"/>
<part name="C1" library="Thomas" deviceset="CAPACITOR_CERAMIC" device="THRUHOLE_P254" value="104"/>
<part name="C2" library="Thomas" deviceset="CAPACITOR_CERAMIC" device="THRUHOLE_P254" value="104"/>
<part name="R1" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="10K"/>
<part name="Y1" library="Thomas" deviceset="CRYSTAL_OSCILLATOR" device="TRUHOLE" value="16.000"/>
<part name="C3" library="Thomas" deviceset="CAPACITOR_CERAMIC" device="THRUHOLE_P254" value="22"/>
<part name="C4" library="Thomas" deviceset="CAPACITOR_CERAMIC" device="THRUHOLE_P254" value="22"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="73.66" y="78.74" size="1.778" layer="98">Programming
Uno</text>
<text x="111.76" y="78.74" size="1.778" layer="98">Programming
Nano</text>
<text x="25.4" y="78.74" size="1.778" layer="98">Programmed
ATMega328</text>
<text x="73.66" y="30.48" size="1.778" layer="98">Not connected</text>
<wire x1="71.12" y1="83.82" x2="71.12" y2="33.02" width="0.1524" layer="98"/>
<wire x1="71.12" y1="33.02" x2="101.6" y2="33.02" width="0.1524" layer="98"/>
<wire x1="101.6" y1="33.02" x2="101.6" y2="83.82" width="0.1524" layer="98"/>
<wire x1="101.6" y1="83.82" x2="71.12" y2="83.82" width="0.1524" layer="98"/>
<wire x1="71.12" y1="33.02" x2="71.12" y2="15.24" width="0.1524" layer="98"/>
<wire x1="71.12" y1="15.24" x2="101.6" y2="15.24" width="0.1524" layer="98"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="33.02" width="0.1524" layer="98"/>
<wire x1="58.42" y1="83.82" x2="58.42" y2="15.24" width="0.1524" layer="98"/>
<wire x1="58.42" y1="15.24" x2="-25.4" y2="15.24" width="0.1524" layer="98"/>
<wire x1="-25.4" y1="15.24" x2="-25.4" y2="83.82" width="0.1524" layer="98"/>
<wire x1="-25.4" y1="83.82" x2="58.42" y2="83.82" width="0.1524" layer="98"/>
<wire x1="109.22" y1="83.82" x2="109.22" y2="33.02" width="0.1524" layer="98"/>
<wire x1="109.22" y1="33.02" x2="109.22" y2="15.24" width="0.1524" layer="98"/>
<wire x1="109.22" y1="15.24" x2="142.24" y2="15.24" width="0.1524" layer="98"/>
<wire x1="142.24" y1="15.24" x2="142.24" y2="33.02" width="0.1524" layer="98"/>
<wire x1="142.24" y1="33.02" x2="142.24" y2="83.82" width="0.1524" layer="98"/>
<wire x1="142.24" y1="83.82" x2="109.22" y2="83.82" width="0.1524" layer="98"/>
<wire x1="109.22" y1="33.02" x2="142.24" y2="33.02" width="0.1524" layer="98"/>
<text x="111.76" y="30.48" size="1.778" layer="98">Not connected</text>
</plain>
<instances>
<instance part="ATMEGA328" gate="G$1" x="25.4" y="50.8" smashed="yes">
<attribute name="NAME" x="25.4" y="68.834" size="1.27" layer="95" align="bottom-center"/>
</instance>
<instance part="MISO" gate="G$1" x="78.74" y="48.26" smashed="yes">
<attribute name="NAME" x="81.788" y="48.26" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="SCK" gate="G$1" x="78.74" y="53.34" smashed="yes">
<attribute name="NAME" x="81.788" y="53.34" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="VCC" gate="G$1" x="78.74" y="63.5" smashed="yes">
<attribute name="NAME" x="81.788" y="63.5" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="MOSI" gate="G$1" x="78.74" y="43.18" smashed="yes">
<attribute name="NAME" x="81.788" y="43.18" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="GND" gate="G$1" x="78.74" y="38.1" smashed="yes">
<attribute name="NAME" x="81.788" y="38.1" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="UNO_D10" gate="G$1" x="78.74" y="58.42" smashed="yes">
<attribute name="NAME" x="81.788" y="58.42" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="UNO_SUPPORT" gate="G$1" x="78.74" y="20.32" smashed="yes">
<attribute name="NAME" x="81.788" y="20.32" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="RESET" gate="G$1" x="78.74" y="25.4" smashed="yes">
<attribute name="NAME" x="81.788" y="25.4" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="NANO_SUPPORT" gate="G$1" x="116.84" y="20.32" smashed="yes">
<attribute name="NAME" x="119.888" y="20.32" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="NANO_D10" gate="G$1" x="119.38" y="58.42" smashed="yes">
<attribute name="NAME" x="122.428" y="58.42" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="C1" gate="G$1" x="10.16" y="53.34" smashed="yes" rot="R90">
<attribute name="VALUE" x="11.684" y="54.61" size="1.27" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="C2" gate="G$1" x="40.64" y="50.8" smashed="yes" rot="R90">
<attribute name="VALUE" x="42.164" y="52.07" size="1.27" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R1" gate="G$1" x="7.62" y="66.04" smashed="yes">
<attribute name="NAME" x="7.62" y="67.31" size="1.778" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="7.62" y="64.77" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="Y1" gate="G$1" x="-15.24" y="43.18" smashed="yes" rot="R90">
<attribute name="VALUE" x="-15.24" y="43.18" size="1.27" layer="96" rot="R180" align="center"/>
</instance>
<instance part="C3" gate="G$1" x="5.08" y="33.02" smashed="yes" rot="R90">
<attribute name="VALUE" x="6.604" y="34.29" size="1.27" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="C4" gate="G$1" x="-2.54" y="33.02" smashed="yes" rot="R90">
<attribute name="VALUE" x="-1.016" y="34.29" size="1.27" layer="96" rot="R90" align="top-center"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="RESET" class="0">
<segment>
<pinref part="UNO_D10" gate="G$1" pin="A"/>
<pinref part="ATMEGA328" gate="G$1" pin="1"/>
<wire x1="76.2" y1="58.42" x2="60.96" y2="58.42" width="0.1524" layer="91"/>
<wire x1="60.96" y1="58.42" x2="60.96" y2="71.12" width="0.1524" layer="91"/>
<wire x1="60.96" y1="71.12" x2="17.78" y2="71.12" width="0.1524" layer="91"/>
<wire x1="17.78" y1="71.12" x2="17.78" y2="66.04" width="0.1524" layer="91"/>
<pinref part="NANO_D10" gate="G$1" pin="A"/>
<wire x1="76.2" y1="58.42" x2="116.84" y2="58.42" width="0.1524" layer="91"/>
<junction x="76.2" y="58.42"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="12.7" y1="66.04" x2="17.78" y2="66.04" width="0.1524" layer="91"/>
<junction x="17.78" y="66.04"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="ATMEGA328" gate="G$1" pin="18"/>
<wire x1="33.02" y1="40.64" x2="63.5" y2="40.64" width="0.1524" layer="91"/>
<wire x1="63.5" y1="40.64" x2="63.5" y2="48.26" width="0.1524" layer="91"/>
<pinref part="MISO" gate="G$1" pin="A"/>
<wire x1="63.5" y1="48.26" x2="76.2" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="ATMEGA328" gate="G$1" pin="19"/>
<wire x1="33.02" y1="43.18" x2="60.96" y2="43.18" width="0.1524" layer="91"/>
<wire x1="60.96" y1="43.18" x2="60.96" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SCK" gate="G$1" pin="A"/>
<wire x1="60.96" y1="53.34" x2="76.2" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="ATMEGA328" gate="G$1" pin="17"/>
<pinref part="MOSI" gate="G$1" pin="A"/>
<wire x1="33.02" y1="38.1" x2="66.04" y2="38.1" width="0.1524" layer="91"/>
<wire x1="66.04" y1="38.1" x2="66.04" y2="43.18" width="0.1524" layer="91"/>
<wire x1="66.04" y1="43.18" x2="76.2" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="A"/>
<pinref part="ATMEGA328" gate="G$1" pin="20"/>
<wire x1="40.64" y1="45.72" x2="33.02" y2="45.72" width="0.1524" layer="91"/>
<wire x1="40.64" y1="45.72" x2="50.8" y2="45.72" width="0.1524" layer="91"/>
<junction x="40.64" y="45.72"/>
<pinref part="ATMEGA328" gate="G$1" pin="7"/>
<pinref part="VCC" gate="G$1" pin="A"/>
<wire x1="17.78" y1="50.8" x2="15.24" y2="50.8" width="0.1524" layer="91"/>
<wire x1="15.24" y1="50.8" x2="15.24" y2="60.96" width="0.1524" layer="91"/>
<wire x1="15.24" y1="60.96" x2="15.24" y2="91.44" width="0.1524" layer="91"/>
<wire x1="15.24" y1="91.44" x2="50.8" y2="91.44" width="0.1524" layer="91"/>
<wire x1="50.8" y1="91.44" x2="63.5" y2="91.44" width="0.1524" layer="91"/>
<wire x1="63.5" y1="91.44" x2="63.5" y2="63.5" width="0.1524" layer="91"/>
<wire x1="63.5" y1="63.5" x2="76.2" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="B"/>
<wire x1="10.16" y1="60.96" x2="15.24" y2="60.96" width="0.1524" layer="91"/>
<junction x="15.24" y="60.96"/>
<wire x1="50.8" y1="45.72" x2="50.8" y2="91.44" width="0.1524" layer="91"/>
<junction x="50.8" y="91.44"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="2.54" y1="66.04" x2="2.54" y2="91.44" width="0.1524" layer="91"/>
<wire x1="2.54" y1="91.44" x2="15.24" y2="91.44" width="0.1524" layer="91"/>
<junction x="15.24" y="91.44"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="ATMEGA328" gate="G$1" pin="22"/>
<wire x1="33.02" y1="50.8" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
<wire x1="35.56" y1="50.8" x2="35.56" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="B"/>
<wire x1="35.56" y1="58.42" x2="40.64" y2="58.42" width="0.1524" layer="91"/>
<wire x1="40.64" y1="58.42" x2="48.26" y2="58.42" width="0.1524" layer="91"/>
<junction x="40.64" y="58.42"/>
<pinref part="ATMEGA328" gate="G$1" pin="8"/>
<wire x1="15.24" y1="48.26" x2="17.78" y2="48.26" width="0.1524" layer="91"/>
<wire x1="15.24" y1="48.26" x2="15.24" y2="5.08" width="0.1524" layer="91"/>
<wire x1="15.24" y1="5.08" x2="48.26" y2="5.08" width="0.1524" layer="91"/>
<pinref part="GND" gate="G$1" pin="A"/>
<wire x1="48.26" y1="5.08" x2="68.58" y2="5.08" width="0.1524" layer="91"/>
<wire x1="76.2" y1="38.1" x2="68.58" y2="38.1" width="0.1524" layer="91"/>
<wire x1="68.58" y1="38.1" x2="68.58" y2="5.08" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="A"/>
<wire x1="10.16" y1="48.26" x2="15.24" y2="48.26" width="0.1524" layer="91"/>
<junction x="15.24" y="48.26"/>
<wire x1="48.26" y1="58.42" x2="48.26" y2="5.08" width="0.1524" layer="91"/>
<junction x="48.26" y="5.08"/>
<pinref part="C3" gate="G$1" pin="A"/>
<wire x1="5.08" y1="27.94" x2="5.08" y2="5.08" width="0.1524" layer="91"/>
<wire x1="5.08" y1="5.08" x2="15.24" y2="5.08" width="0.1524" layer="91"/>
<junction x="15.24" y="5.08"/>
<pinref part="C4" gate="G$1" pin="A"/>
<wire x1="-2.54" y1="27.94" x2="-2.54" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="5.08" x2="5.08" y2="5.08" width="0.1524" layer="91"/>
<junction x="5.08" y="5.08"/>
</segment>
</net>
<net name="CRYSTAL2" class="0">
<segment>
<wire x1="-7.62" y1="43.18" x2="-7.62" y2="35.56" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="A"/>
<wire x1="-7.62" y1="35.56" x2="-15.24" y2="35.56" width="0.1524" layer="91"/>
<pinref part="ATMEGA328" gate="G$1" pin="10"/>
<wire x1="-7.62" y1="43.18" x2="-2.54" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="B"/>
<wire x1="-2.54" y1="43.18" x2="17.78" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="40.64" x2="-2.54" y2="43.18" width="0.1524" layer="91"/>
<junction x="-2.54" y="43.18"/>
</segment>
</net>
<net name="CRYSTAL1" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="B"/>
<wire x1="-7.62" y1="45.72" x2="-7.62" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="50.8" x2="-15.24" y2="50.8" width="0.1524" layer="91"/>
<pinref part="ATMEGA328" gate="G$1" pin="9"/>
<wire x1="17.78" y1="45.72" x2="5.08" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="B"/>
<wire x1="5.08" y1="45.72" x2="-7.62" y2="45.72" width="0.1524" layer="91"/>
<wire x1="5.08" y1="40.64" x2="5.08" y2="45.72" width="0.1524" layer="91"/>
<junction x="5.08" y="45.72"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="101,1,76.2,20.32,UNO_SUPPORT,A,,,,"/>
<approved hash="101,1,76.2,25.4,RESET,A,,,,"/>
<approved hash="101,1,114.3,20.32,NANO_SUPPORT,A,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
